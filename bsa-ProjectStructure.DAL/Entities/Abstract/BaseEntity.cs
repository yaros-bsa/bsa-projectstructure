﻿using System;
namespace bsa_ProjectStructure.DAL.Entities.Abstract
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
    }
}
