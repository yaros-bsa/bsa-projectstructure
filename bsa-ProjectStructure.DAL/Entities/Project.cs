﻿using System;
using bsa_ProjectStructure.DAL.Entities.Abstract;

namespace bsa_ProjectStructure.DAL.Entities
{
    public class Project : BaseEntity
    {
        public int AuthorId { get; set; }
        public int? TeamId { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public DateTime? Deadline { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
