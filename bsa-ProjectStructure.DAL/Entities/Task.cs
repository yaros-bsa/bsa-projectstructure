﻿using System;
using bsa_ProjectStructure.DAL.Entities.Abstract;

namespace bsa_ProjectStructure.DAL.Entities
{
    public class Task : BaseEntity
    {
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }

        public TaskState State { get; set; }

        public DateTime? CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }

    public enum TaskState
    {
        Start,
        Finish,
        Lock,
        Cancel
    }
}
