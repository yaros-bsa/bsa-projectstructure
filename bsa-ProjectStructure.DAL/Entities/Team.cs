﻿using System;
using bsa_ProjectStructure.DAL.Entities.Abstract;

namespace bsa_ProjectStructure.DAL.Entities
{
    public class Team : BaseEntity
    {
        public string Name { get; set; }

        public DateTime? CreatedAt { get; set; }
    }
}
