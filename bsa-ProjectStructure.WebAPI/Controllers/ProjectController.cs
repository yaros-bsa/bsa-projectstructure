﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using bsaProjectStructure.BLL.DTO;
using bsaProjectStructure.BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace bsa_ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    public class ProjectController : Controller
    {
        private readonly IProjectRepository _projectRepository;

        public ProjectController(IProjectRepository projectRepository)
        {
            _projectRepository = projectRepository;
        }


        [HttpGet("{id}")]
        public ProjectDTO Get(int id)
        {
            return _projectRepository.GetById(id);
        }

        [HttpGet]
        public IEnumerable<ProjectDTO> Get()
        {
            return _projectRepository.GetAll();
        }


        [HttpPost]
        public void Post([FromBody] ProjectDTO entity)
        {
            _projectRepository.Create(entity);
        }


        [HttpPut("{id}")]
        public void Put([FromBody] ProjectDTO entity)
        {
            _projectRepository.Update(entity);
        }


        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _projectRepository.Delete(id);
        }
    }
}
