﻿using System;
using System.Collections.Generic;
using bsa_ProjectStructure.DAL.Entities;
using bsaProjectStructure.BLL.DTO;

namespace bsaProjectStructure.BLL.Interfaces
{
    public interface IProjectRepository : IRepository<Project>
    {
        ProjectDTO GetById(int id);

        IEnumerable<ProjectDTO> GetAll();

        void Create(ProjectDTO entity);

        void Update(ProjectDTO entity);

        void Delete(int id);
    }
}
