﻿using System;
using System.Collections.Generic;
using bsa_ProjectStructure.DAL.Entities;
using bsaProjectStructure.BLL.DTO;
using bsaProjectStructure.BLL.Interfaces;

namespace bsaProjectStructure.BLL.Services
{
    public class ProjectRepository : IRepository<Project>, IProjectRepository
    {
        public void Create(ProjectDTO entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ProjectDTO> GetAll()
        {
            throw new NotImplementedException();
        }

        public ProjectDTO GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(ProjectDTO entity)
        {
            throw new NotImplementedException();
        }
    }
}
